## How to run the App?

1. `npm install`
2. `npm start`

Please open [http://localhost:3000](http://localhost:3000) 

### Tests

Please run `npm test` I have created 9 unit tests for components and helpers functions.

### More commands:

1. To run eslint: `npm run lint`
2. To run prettier `npm run format`
3. To make production build `npm run build`

### Description
**CSS:**
I used css modules with variables (custom props). 
I like to use clean css, and that is why I did not use inheritance, I tried to re-use the code/rules as much as possible, and did not go very deep into variables, I believe we need to find a balance.

**Javascript/TypeScript:**  You will find 3 components: input, comments and button for vote 
- I putted focus on performance because of recursion, used `useCallback`, `React.memo`, 
- I could create more components for example: for another buttons, but it's better to not over engineer.
- App.js contains `initialComments` array, which is not necessary and could be removed as well (just wanted to show full UI from the begining). 

Hope you will be able to read and understand my code easily

### Code Style
I like to follow [Airbnb Code Style](https://github.com/airbnb/javascript)

My favorite tools are [Prettier](https://prettier.io/) and [eslint](https://eslint.org/) with extension from Airbnb

PS: I tried not to spend more then **8 hours**

### Thank you Inkitt for React codding challenge. Looking forward to hear your feedback!