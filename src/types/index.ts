export interface Comment {
  author: string;
  id: number;
  replies?: Comment[];
  text: string;
  timestamp: number;
  vote: true | false | null;
}
