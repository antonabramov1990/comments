import React, { useState, useCallback } from 'react';
import cn from 'classnames';
import { IconMessage } from '@tabler/icons-react';
import { CommentInput } from '../commentInput';
import { VoteButton } from './voteButton';
import { getHumanTime } from '../../helpers/getHumanTime';
import type { Comment } from '../../types';
import styles from './commentBlock.module.css';
import avatar from '../../assets/avatar.png';

interface CommentBlockProps {
  comment: Comment;
  isRoot?: boolean;
  // eslint-disable-next-line no-unused-vars
  onChange: (updatedComment: Comment) => void;
  // eslint-disable-next-line no-unused-vars
  onDelete: (commentToDelete: Comment) => void;
}

export const CommentBlock: React.FC<CommentBlockProps> = ({
  comment,
  onChange,
  onDelete,
  isRoot,
}) => {
  const [isReplyFormShown, setIsReplyFormShown] = useState(false);

  const closeReplyForm = () => {
    setIsReplyFormShown(false);
  };

  const addReplyHandler = useCallback(
    (replyComment: Comment) => {
      /* Update the comment's replies array to include a new reply */
      const updatedComment = {
        ...comment,
        replies: [...(comment.replies || []), replyComment],
      };
      onChange(updatedComment);
      closeReplyForm();
    },
    [comment, onChange],
  );

  const replyChangeHandler = useCallback(
    (updatedChildComment: Comment) => {
      /* Update the comment's replies array with the modified nested comment */
      const updatedReplies = comment.replies?.map((c) =>
        c.id === updatedChildComment.id ? updatedChildComment : c,
      );
      onChange({ ...comment, replies: updatedReplies });
    },
    [comment, onChange],
  );

  return (
    <div className={styles.wrapper}>
      <div className={styles.aside}>
        <img
          alt='Avatar'
          className={cn(styles.avatarImg, { [styles.avatarImgRoot]: isRoot })}
          src={avatar}
        />
        <div className={styles.divider} />
      </div>
      <div className={styles.content}>
        <header className={styles.header}>
          <p className={styles.name}>{comment.author}</p>
          <time className={styles.date}>{getHumanTime(comment.timestamp)}</time>
        </header>
        <p className={styles.comment}>{comment.text}</p>
        <footer className={styles.footer}>
          <VoteButton vote={comment.vote} />
          <button
            className={styles.button}
            data-testid='reply'
            onClick={() => setIsReplyFormShown(true)}
            type='button'
          >
            <IconMessage className={styles.commentIcon} size={18} />
            <p>Reply</p>
          </button>
          <button
            className={styles.button}
            data-testid='delete'
            onClick={() => onDelete(comment)}
            type='button'
          >
            Delete
          </button>
        </footer>
        {isReplyFormShown && (
          <CommentInput buttonText='Reply' onCancel={closeReplyForm} onSubmit={addReplyHandler} />
        )}
        <div>
          {comment.replies?.map((reply) => (
            <MemoizedCommentBlock
              comment={reply}
              key={reply.id}
              onChange={replyChangeHandler}
              onDelete={onDelete}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

// to improve performance and re-renders.
const MemoizedCommentBlock = React.memo(CommentBlock);
