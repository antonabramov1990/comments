import React, { useState } from 'react';
import cn from 'classnames';
import { IconArrowBigUp, IconArrowBigUpFilled } from '@tabler/icons-react';
import styles from './commentBlock.module.css';

interface VoteButtonProps {
  vote: boolean | null;
}

export const VoteButton: React.FC<VoteButtonProps> = ({ vote }) => {
  const [voteActive, setVoteActive] = useState<boolean | null>(vote);
  let voteRating = '0';
  if (voteActive === true) {
    voteRating = '1';
  } else if (voteActive === false) {
    voteRating = '-1';
  }

  const toggleVote = (value: boolean | null) => {
    setVoteActive((prevValue) => (prevValue === value ? null : value));
  };

  return (
    <div className={styles.vote}>
      <button className={styles.button} onClick={() => toggleVote(true)} type='button'>
        {voteRating === '1' ? (
          <IconArrowBigUpFilled className={cn(styles.buttonIcon, styles.buttonIconActive)} />
        ) : (
          <IconArrowBigUp className={cn(styles.buttonIcon)} />
        )}
      </button>
      <p className={styles.voteCount}>{voteRating}</p>
      <button className={styles.button} onClick={() => toggleVote(false)} type='button'>
        {voteRating === '-1' ? (
          <IconArrowBigUpFilled
            className={cn(styles.buttonIcon, styles.buttonIconActive, styles.buttonIconRotate)}
          />
        ) : (
          <IconArrowBigUp className={cn(styles.buttonIcon, styles.buttonIconRotate)} />
        )}
      </button>
    </div>
  );
};
