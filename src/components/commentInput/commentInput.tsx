import React, { useState } from 'react';
import cn from 'classnames';
import type { Comment } from '../../types';
import styles from './commentInput.module.css';

interface CommentInputProps {
  buttonText?: string;
  // eslint-disable-next-line no-unused-vars
  onSubmit: (comment: Comment) => void;
  onCancel?: () => void;
}

export const CommentInput: React.FC<CommentInputProps> = ({
  onSubmit,
  onCancel,
  buttonText = 'Comment',
}) => {
  const [text, setText] = useState('');
  const [commentSubmission, setCommentSubmission] = useState(false);

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    // we need to disable the button that user would not click several times during the submission
    // on UI it's almost not visible that button changes the color to grey, because it's very fast
    // but if we would have delay or promise here, user would see this UX improvement.
    if (text.trim().length === 0) {
      // eslint-disable-next-line no-alert
      alert("Comment can't be empty or contains only white spaces.");
      return;
    }
    setCommentSubmission(true);
    const id = new Date().getTime();
    onSubmit({
      id,
      text,
      author: 'Anton Abramov',
      vote: null,
      timestamp: Date.now(),
    });
    setText('');
    setCommentSubmission(false);
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <textarea
        autoFocus
        className={styles.input}
        onChange={(e) => setText(e.target.value)}
        onKeyDown={(event) => {
          if (event.key === 'Enter' && event.shiftKey === false) {
            event.preventDefault();
            handleSubmit(event);
          }
        }}
        placeholder='What are your thoughts?'
        required
        value={text}
      />
      <footer className={styles.footer}>
        {!!onCancel && (
          <button
            className={cn(styles.btn, styles.btnCancel)}
            data-testid='cancel'
            onClick={onCancel}
            type='button'
          >
            Cancel
          </button>
        )}
        <button
          className={cn(styles.btn, styles.btnSubmit)}
          data-testid='submit'
          disabled={commentSubmission}
          type='submit'
        >
          {buttonText}
        </button>
      </footer>
    </form>
  );
};
